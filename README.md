# A complete shell image to test you kubernetes cluster

This repo contains clients for:
 - MongoDB
 - Mysql
 - Redis

Also contains needed packages like:
 - curl
 - wget
 - htop
 - nano

In the user dir there is algo a sh with a few helpers like:
 - Get Cluster Public IP

# Deploy with my own image

To deploy with your image just transfer this repo to your own, fork it, clone it,...
Then once you finish all your changes execute de following commands to build push a new image to your registry (in this case I'm using gitlab):

> docker-compose build
>
> docker tag shell_bash:latest registry.gitlab.com/manther02/kube-shell:{tag}
>
> docker push registry.gitlab.com/manther02/kube-shell:{tag}

Notice that `{tag}` should be the image tag you want to setup in your registry, it can be `1.2.3` (in this case I advise to push 2 images, the version and the `latest`) or just push `latest` and it will overwrite any images with the same tag.