FROM debian:stable-20201209-slim

RUN apt update
RUN apt install gnupg wget curl nano htop mariadb-client redis-tools -y

RUN wget https://downloads.mongodb.org/linux/mongodb-shell-linux-x86_64-debian10-4.4.2.tgz
RUN tar -xvzf mongodb-shell-linux-x86_64-debian10-4.4.2.tgz
RUN mv /mongodb-linux-x86_64-debian10-4.4.2/bin/mongo /usr/bin/mongo

COPY ./helper.sh /helper.sh
RUN chmod +x /helper.sh

ENTRYPOINT while true; do sleep 60; done