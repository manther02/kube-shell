#!/bin/bash

# Menu
function menu {
    option=""
    while [ "$option" == "" ];do
        echo "Menu:"
        echo "1 - Show Public IP"
        echo "0 - Exit"

        read -n 2 -p ":" option

        if [ "$option" != "0" ] && [ "$option" != "1" ];then
            option=""
        fi
        clear
        echo "Invalid Option!"
    done

    clear
    
    if [ "$option" == "1" ];then
        show_public_ip
    fi

    exit
}

function show_public_ip {
    echo "Your Public IP is:"
    curl ifconfig.co
}

menu